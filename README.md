# IR Client

This python app requires Python 3

This app requires the IR Control Snips voice platform skill https://bitbucket.org/oziee/ircontrol  

The IR Client app runs on RPi. It has access to an IR Transmitter LED and IR Receiver

### Update the Raspberry Pi
_____
```
$ sudo apt-get update
$ sudo apt-get upgrade
```

### Install git and pip to install other python libs we will need
_____
```
$ sudo apt-get install -y python-pip
$ sudo apt-get install -y python-git
```

### Download IR Control's Client and Install
_____
This will download the github for this project
It will then install required libraries and create a service so the app starts on startup

```
$ git clone https://oziee@bitbucket.org/oziee/irclient.git
$ cd irclient
$ sudo bash install.sh

```
make sure you edit the config.ini file with your device name, and MQTT settings

# IR Tx/Rx Setup
_____
The default install sets the in/out pins for the Tx/Rx
/boot/config.txt
```
dtoverlay=lirc-rpi,gpio_out_pin=17,gpio_in_pin=18,gpio_in_pull=up
```
### Test lirc is working
_____
```
$ sudo /etc/init.d/lircd stop
$ mode2 -d /dev/lirc0
<press a key in remote and you should see multple lines like below>
pulse 560
space 1706
pulse 535
```
### Add a new remote and codes
_____
Manually setting up your own remotes
```
$ sudo /etc/init.d/lircd stop
$ sudo irrecord -d /dev/lirc0 ~/lircd.conf
```
follow the instruction as prompted by lirc
at the end ~/<remote_name>.lircd.conf file will be generated
```
$ sudo cp ~/<remote_name>.lircd.conf /etc/lirc/lircd.conf.d
$ sudo /etc/init.d/lircd start
```

### lirc commands
_____
To see which remotes lirc knows  
`$ irsend LIST "" ""`

If you have remotes created for lirc it will list these
to then check programmed buttons for a remote  
`$ irsend LIST <REMOTE> "" `

There is plenty of resources out there for lirc. Sesrch google or have a look at the lirc site http://www.lirc.org

lirc also has a depo of remotes that others have submitted that you can download and use  
http://lirc.sourceforge.net/remotes/  
You can read here how to search, and get a remote from the depo here http://www.lirc.org/html/irdb-get.html


### Wiring
_____
![wiring](https://bitbucket.org/oziee/irclient/raw/b21e3f781055601790a42855f27235d8b85b2648/images/pinout.jpg)

![fritz](https://bitbucket.org/oziee/irclient/raw/b21e3f781055601790a42855f27235d8b85b2648/images/fritzing.jpg)

![circuit](https://bitbucket.org/oziee/irclient/raw/b21e3f781055601790a42855f27235d8b85b2648/images/circuit.jpg)

![receiver](https://bitbucket.org/oziee/irclient/raw/b21e3f781055601790a42855f27235d8b85b2648/images/receiver.jpg)

![transmitter](https://bitbucket.org/oziee/irclient/raw/b21e3f781055601790a42855f27235d8b85b2648/images/transmitter.jpg)