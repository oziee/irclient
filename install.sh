#/usr/bin/env bash -e
echo "Installing... Sit back it will take a minute"

sudo pip install virtualenv

VENV=venv

if [ ! -d "$VENV" ]
then

    PYTHON=`which python3`

    if [ ! -f $PYTHON ]
    then
        echo "could not find python"
    fi
    virtualenv -p $PYTHON $VENV

fi

. $VENV/bin/activate

pip install -r requirements.txt

sudo apt-get install -y lirc

sudo sed -i 's+#dtoverlay=lirc-rpi+dtoverlay=lirc-rpi,gpio_out_pin=17,gpio_in_pin=18,gpio_in_pull=up+' /boot/config.txt
sudo sed -i 's/devinput/default/' /etc/lirc/lirc_options.conf
sudo sed -i 's+auto+/dev/lirc0+' /etc/lirc/lirc_options.conf

sudo rm /etc/lirc/lircd.conf.d/devinput.lircd.conf

sudo cp IRClient.service /lib/systemd/system/IRClient.service
chmod +x ir_client.py
chmod +x run.sh
sudo systemctl daemon-reload
sudo systemctl enable IRClient.service
sudo systemctl start IRClient.service

echo ""
echo ""
echo ""
echo ""
echo "Install Complete!"
echo ""
echo "edit the settings.ini file to match your mqtt settings 'nano settings.ini'"

echo ""
echo "Unplug the Pi from the power.. Connect up your IR Tx/Rx as outlined in the README if not already done"
echo "Then plug it back in.. and start setting up remotes"
echo "Time to reboot.."