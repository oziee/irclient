#!/usr/bin/env python3
# -*- coding:utf-8 -*-

### **************************************************************************** ###
# 
# Project: Snips Web Admin
# Created Date: Saturday, April 6th 2019, 7:07:17 pm
# Author: Greg
# -----
# Last Modified: Fri Apr 19 2019
# Modified By: Greg
# -----
# Copyright (c) 2019 Greg
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
# AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# 
### **************************************************************************** ###




import configparser
import paho.mqtt.client as mqtt
import subprocess
import json
import io
import time

CONFIGURATION_ENCODING_FORMAT = "utf-8"
CONFIG_INI = "settings.ini"

def read_configuration_file(configuration_file):
    try:
        with io.open(configuration_file, encoding=CONFIGURATION_ENCODING_FORMAT) as f:
            conf_parser = configparser.RawConfigParser()
            conf_parser.optionxform = str 
            conf_parser.read_file(f)
            return conf_parser
    except (IOError, configparser.Error) as e:
        config = configparser.RawConfigParser()
        config.optionxform = str 
        config['DEFAULT'] = {'NAME': 'NoName', 'MQTT_BROKER': 'localhost', 'MQTT_PORT': 1883}
        return config

conf = read_configuration_file(CONFIG_INI)

client_name = conf['DEFAULT']['NAME']


client = mqtt.Client(client_name)



def on_disconnect(client, userdata, rc):
    #print("client disconnected")
    client.connected_flag=False
    client.disconnected_flag=True

def on_connect(Client, userdate, flag, rc):
    #print("client connected")
    client.connected_flag=True
    client.disconnected_flag=False
    client.subscribe('ir_control/hello')
    client.subscribe('ir_control/{}'.format(client_name))

def on_message(client, userdata, message):
    if message.topic == 'ir_control/hello':
        cmd = 'irsend list "" ""'
        p = subprocess.Popen(cmd, shell=True, universal_newlines=True, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
        out, err = p.communicate()
        out = out.split('\n')
        if err:
            client.publish('ir_control/info',json.dumps({'device':client_name,'error':True,'data':[]}))
        elif out:
            remoteArray = []
            remotes = out 
            remotes = [x for x in remotes if x != '']

            for remote in remotes:
                cmd = 'irsend list {} ""'.format(remote)
                p = subprocess.Popen(cmd, shell=True, universal_newlines=True, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
                out, err = p.communicate()
                if not err:
                    out = out.split('\n')
                    out = [x for x in out if x != '']
                    functions = []
                    for funcs in out:
                        functions.append((funcs.split(' ')[1]))

                    remoteArray.append({remote:functions})
            #now we need to get a list of all the commands for each remote
            #print({'device':client_name,'error':False,'data':remoteArray})
            client.publish('ir_control/info',json.dumps({'device':client_name,'error':False,'data':remoteArray}))
        else:
            client.publish('ir_control/info',json.dumps({'device':client_name,'error':True,'data':[]}))
    else:
        #run the command
        #{"remote": "television", "function": "KEY_POWER", "hold": false, "holdtime": 0, "pause": 0}
        #irsend SEND_ONCE PANASONIC KEY_VOLUMEDOWN
        
        data = json.loads(message.payload.decode())
        #print("Run IR Command - {}".format(data))
        
        if data['hold'] == True:
            cmd = 'irsend SEND_START {} {}'.format(data['remote'],data['function'])
            p = subprocess.Popen(cmd, shell=True, universal_newlines=True, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
            out, err = p.communicate()
            #print('out',out)
            #print('err',err)
            time.sleep(data['holdtime'])
            cmd = 'irsend SEND_STOP {} {}'.format(data['remote'],data['function'])
            p = subprocess.Popen(cmd, shell=True, universal_newlines=True, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
            out, err = p.communicate()
            #print('out',out)
            #print('err',err)
        else:
            cmd = 'irsend SEND_ONCE {} {}'.format(data['remote'],data['function'])
            p = subprocess.Popen(cmd, shell=True, universal_newlines=True, stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
            out, err = p.communicate()
            #print('out',out)
            #print('err',err)
            
        if data['pause'] > 0:
            time.sleep(data['pause'])
        time.sleep(0.2)



if __name__ == "__main__":
    client.on_connect = on_connect
    client.on_message = on_message
    client.on_disconnect = on_disconnect
    client.connected_flag=False
    client.disconnected_flag=True

    url = str(conf['DEFAULT']['MQTT_BROKER'])
    port = int(conf['DEFAULT']['MQTT_PORT'])

    client.connect(url, port=port, keepalive=120, bind_address="")
    client.loop_forever()